#!/usr/bin/env bash
#
# The MIT License (MIT)
# Copyright (c) Curtis <curtis@flynax.com>
# The script allow to make the same changes for more repositories located in current directory via one operation
# For example: templates, packages and etc.

RED='\033[0;31m'
BROW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

BRANCH='develop'
MODE='SHOW_CHANGES'
# You can use following values for MODE option:
# 1.  SHOW_CHANGES         - Git will show uncommitted changes in selected branch
# 2.  ADD_COMMIT           - Add commit to selected branch after you make the changes in files
# 3.  REVERT_LAST_COMMIT   - Use it to remove previous commit in local branch (if you make a mistake for example)
# 4.  PUSH                 - Git make a push
# 5.  CREATE_BRANCH        - Git create local branch
# 6.  CREATE_REMOTE_BRANCH - Git will create local branch and push it to origin
# 7.  PULL                 - Get new commits in "$BRANCH" and move local changes up if they exists
# 8.  PULL_AND_RESET       - Get new commits in "$BRANCH" and remove all local branches except "$BRANCH"
# 9.  MERGE                - Merge changes from "$BRANCH" to develop and close MR; command will remove local and remote branches
# 10. CHANGE_BRANCH        - Git change branch to "$BRANCH" and update it
# 11. GULP_SASS            - Script run command "gulp sass" for all internal repos
# 12. CONFIGURE_LOCAL_GULP - Script configure GULP for all internal repos

# EXAMPLE of scenario when you want to make changes in all repos
# 1. set branch to "develop" and PULL all changes
# 2. add new branch name and create local branch CREATE_BRANCH
# 3. add changes in files and add commit to branch ADD_COMMIT
# 4. create remote branch with all commits CREATE_REMOTE_BRANCH
# ----------
# EXAMPLE of scenario when you want to make changes in selective repos
# 1. set branch to "develop" and PULL all changes
# 2. create necessary branch via console or program like GitKraken (not via this script)
# 3. add branch name to this script
# 4. add changes in files and add commit to branch ADD_COMMIT
# 5. create remote branch with all commits CREATE_REMOTE_BRANCH (script will create branches in those repos where branch exist)

COMMIT_MESSAGE='Text of commit'

for folder in `find . -maxdepth 1 -type d`; do
    if [[ $folder == '.' || $folder == './.idea' ]]; then
        continue
    fi

    printf "$RED `basename $folder`" ;
    cd $folder
    echo

    CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`
    CHANGES=`git status --porcelain` # git status --porcelain || git diff --name-status
    EXIST_CHANGES=`git status --porcelain` # git status --porcelain || git diff --name-status

    if [[ $BRANCH == $CURRENT_BRANCH
        || $MODE == 'PULL_AND_RESET'
        || $MODE == 'CREATE_BRANCH'
        || $MODE == 'CHANGE_BRANCH'
     ]]; then
        if [[ $MODE == 'PULL' || $MODE == 'CHANGE_BRANCH' ]]; then
            printf "$NC $BROW `git fetch && git checkout $BRANCH && git pull --rebase`"
        fi

        if [[ $MODE == 'PULL_AND_RESET' ]]; then
            printf "$NC $BROW `git fetch && git checkout $BRANCH && git pull && git branch | grep -v $BRANCH | xargs git branch -D`"
        fi

        if [[ $MODE == 'SHOW_CHANGES' ]]; then
            echo
            printf "$NC $BROW CURRENT BRANCH: $BRANCH $NC"
            echo
            printf "$NC $BROW CHANGES: $NC"
            echo
            printf "$NC $BLUE $CHANGES $NC"
        fi

        if [[ $MODE == 'ADD_COMMIT' && $EXIST_CHANGES != '' ]]; then
            printf "$NC $BROW`git add -A && git commit -m "$COMMIT_MESSAGE"` $NC"
        fi

        if [[ $MODE == 'REVERT_LAST_COMMIT' && $COMMIT_MESSAGE == `git log -1 --pretty=%B` ]]; then
            printf "$NC $BROW`git reset HEAD^` $NC"
        fi

        if [[ $MODE == 'PUSH' && `git cherry -v` != '' ]]; then
            printf "$NC $BROW`git push` $NC"
        fi

        if [[ $MODE == 'CREATE_BRANCH' ]]; then
            printf "$NC $BLUE $NC `git branch $BRANCH && git checkout $BRANCH`"
        fi

        if [[ $MODE == 'CREATE_REMOTE_BRANCH' ]]; then
            echo
            printf "$NC $BLUE $NC `git push -u origin $BRANCH`"
        fi

        if [[ $MODE == 'MERGE' ]]; then
            # merge changes and remove local and remote branches
            printf "$NC $BROW `git checkout develop && git pull --rebase && git merge --no-ff $BRANCH && git push origin develop && sleep 3 && git push origin --delete $BRANCH && git branch -d $BRANCH`"
        fi

        if [[ $MODE == 'GULP_SASS' && $folder != './template_core' ]]; then
            # update CSS files
            printf "$NC $BROW `gulp sass`"
        fi

        if [[ $MODE == 'CONFIGURE_LOCAL_GULP' && $folder != './template_core' ]]; then
            # printf "$NC $BROW `sudo npm install npm@latest -g`"
            printf "$NC $BROW `npm install`"
        fi
    else
        printf "$NC $BLUE Repo use another branch: $NC $CURRENT_BRANCH"

        # if [[ $MODE == 'CREATE_REMOTE_BRANCH' ]]; then
        #     echo
        #     git checkout develop && git pull --rebase
        #     printf "$NC $BLUE $NC `git checkout -b $BRANCH && git push -u origin $BRANCH`"
        # fi
    fi

    cd ..
    echo
    echo
done

printf "$NC $BLUE----------------------------------------$NC";
echo
echo ;
printf "$BROW FINISH $NC";

read
